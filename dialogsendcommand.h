#ifndef DIALOGSENDCOMMAND_H
#define DIALOGSENDCOMMAND_H

#include <QDialog>

namespace Ui {
class DialogSendCommand;
}

class DialogSendCommand : public QDialog
{
    Q_OBJECT
    
public:
    explicit DialogSendCommand(QWidget *parent = 0);
    ~DialogSendCommand();
    
private slots:
    void on_pushButton_clicked();

private:
    Ui::DialogSendCommand *ui;
};

#endif // DIALOGSENDCOMMAND_H
