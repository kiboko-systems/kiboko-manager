#include "dialoggeneralsettings.h"
#include "ui_dialoggeneralsettings.h"
#include <QSettings>
#include <QDebug>



DialogGeneralSettings::DialogGeneralSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogGeneralSettings)
{
	ui->setupUi(this);
	setModal(true);
	
	load();
}


DialogGeneralSettings::~DialogGeneralSettings()
{
    delete ui;
}


void DialogGeneralSettings::load()
{
	QSettings set;
    set.beginGroup(SS_GS_PREFIX);

    qDebug() << "Settings filename: " << set.fileName();

    bool ok;
    int v;
    v = set.value(SS_GS_PUBLISH_INTERVAL, 15).toInt(&ok);
    if(!ok){
        qDebug() << "Error on loading Setting: " << SS_GS_PUBLISH_INTERVAL << ". Using Default Value 15";
    }else{
        qDebug() << "Loaded Setting: " << SS_GS_PUBLISH_INTERVAL << ": " << v;
    }
    ui->spinBox_publishInterval->setValue(v);

    v = set.value(SS_GS_START_INTERVAL, 60).toInt(&ok);
    if(!ok){
        qDebug() << "Error on loading Setting: " << SS_GS_START_INTERVAL << ". Using Default Value 60";
    }else{
        qDebug() << "Loaded Setting: " << SS_GS_START_INTERVAL << ": " << v;
    }
    ui->spinBox_startInterval->setValue(v);


    ui->checkBox_soundOnTrigger->setChecked(set.value(SS_GS_SIGNAL_ON_TRIGGER, true).toBool());
    ui->checkBox_soundOnStart->setChecked(set.value(SS_GS_SIGNAL_ON_START, true).toBool());
	
	ui->checkBox_publishDSQ->setChecked(set.value(SS_GS_PUBLISH_DSQ, true).toBool());
	ui->checkBox_publishErrors->setChecked(set.value(SS_GS_PUBLISH_ERRORS, true).toBool());

    ui->radioButton_assignOnlyForward->setChecked(set.value(SS_GS_ASSIGN_ONLY_FORWARD, true).toBool()); // lower austria
    ui->radioButton_assignToCurrentRound->setChecked(set.value(SS_GS_ASSIGN_TO_CURRENT_ROUND, false).toBool()); // upper austria
}


void DialogGeneralSettings::save()
{
	QSettings set;
    set.beginGroup(SS_GS_PREFIX);

    set.setValue(SS_GS_PUBLISH_INTERVAL, ui->spinBox_publishInterval->value());
    set.setValue(SS_GS_START_INTERVAL, ui->spinBox_startInterval->value());
    set.setValue(SS_GS_SIGNAL_ON_TRIGGER, ui->checkBox_soundOnTrigger->isChecked());
    set.setValue(SS_GS_SIGNAL_ON_START, ui->checkBox_soundOnStart->isChecked());
	set.setValue(SS_GS_PUBLISH_DSQ, ui->checkBox_publishDSQ->isChecked());
	set.setValue(SS_GS_PUBLISH_ERRORS, ui->checkBox_publishErrors->isChecked());
    set.setValue(SS_GS_ASSIGN_ONLY_FORWARD, ui->radioButton_assignOnlyForward->isChecked());
    set.setValue(SS_GS_ASSIGN_TO_CURRENT_ROUND, ui->radioButton_assignToCurrentRound->isChecked());
    qDebug()<< "wrote Settings " << SS_GS_PREFIX;
}


void DialogGeneralSettings::accept()
{
	// if accepted, save settings
	save();
	QDialog::accept();
}


void DialogGeneralSettings::reject()
{
	// if rejected, restore old settings
	load();
	QDialog::reject();
}
