#ifndef DIALOGSENDPHOTOFILTERPARAMS_H
#define DIALOGSENDPHOTOFILTERPARAMS_H

#include <QDialog>
#include "defs.h"
#include "packet.h"
#include <QTimer>

namespace Ui {
class DialogSendPhotoFilterParams;
}

class DialogSendPhotoFilterParams : public QDialog
{
	Q_OBJECT
	
signals:
    void sendRadioCommand(uint16_t cmdReceiver, uint8_t command, uint16_t commandData);
	
public:
	explicit DialogSendPhotoFilterParams(QWidget *parent = 0);
	~DialogSendPhotoFilterParams();
	
private slots:
	void on_buttonBox_accepted();
	
	void on_buttonBox_rejected();
	
	void on_pushButton_defaultSettings_clicked();
	
    void on_pushButton_send_clicked();

    void on_pushButton_save_clicked();

    void on_pushButton_restore_clicked();

private:
	void resetUI();

	Ui::DialogSendPhotoFilterParams *ui;
	
    int boatboxID;
    int synchronousPulseDetection;
    int pulseMemoryLength;
    int pulseDetectionLimit;
    int missingPulseTolerance;
    int pulseJitterTolerance;

    QTimer responseUpdateTimer;

private slots:
    void updateResponseValues();
	
    void on_pushButton_help_clicked();
};

#endif // DIALOGSENDPHOTOFILTERPARAMS_H
