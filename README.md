Kiboko Manager
==============

# Dokumentation im Wiki
https://gitlab.com/KarlZeilhofer/kiboko-manager/wikis/home



# Change Log

## V17.0.2
Nach dem Kurztest in Zwentendorf gibt es hier noch folgende Änderungen:
* aktuelle Screenshots
* Doku im GitLab Wiki: https://gitlab.com/KarlZeilhofer/kiboko-manager/wikis/home
* Menüeinträge angepasst
  Debug Menü nun unter Hilfe
  Umbennenung: Upload auf Synchronisieren mit FDISK und Upload
* debug-kiboko.sh um `mkdir logs` erweitert
* Standardwerte für Photo-Filter angepasst

## V17.0.1
Wurde mit dem Frühjahr-Service 2017 veröffentlicht

* Boot-Boxen haben einstellbaren Filter (Menü - Debug - Set Photofilter Parameters)
* DSQ und Fehler sind in der Veröffentlichung abschaltbar (Menü - Einstellungen - Allgemeines)
* Fix vom FDISK-Bug, der besonders beim Bundesbewerb zu Fehlverhalten führte.
* Zwischenwertung kann nun per Shortcut hochgeladen werden (Strg+U)
* Vollständiges Logging auch bei Anwendungs-Crash. Debug-Version ist dazu notwendig.
  Gestartet werden muss die Anwendung über ein Skript (debug-kiboko.sh), das den GDB verwendet und Netzwerkmonitoring betreibt.
* Versionsnummer eingeführt


## V4.3
Alle bisherigen Änderungen, ohne explizite Change-Log-Datei.
